libeuclid-java (2.6-1) unstable; urgency=medium

  * New upstream version 2.6
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.

 -- Andrius Merkys <merkys@debian.org>  Mon, 28 Aug 2023 04:22:45 -0400

libeuclid-java (2.5-1) experimental; urgency=medium

  * New upstream version 2.5
  * Update standards version to 4.6.2, no changes needed.
  * Bump copyright years.

 -- Andrius Merkys <merkys@debian.org>  Thu, 13 Apr 2023 01:56:39 -0400

libeuclid-java (2.3-1) unstable; urgency=medium

  * New upstream version 2.3

 -- Andrius Merkys <merkys@debian.org>  Tue, 23 Aug 2022 03:14:16 -0400

libeuclid-java (2.1-1) unstable; urgency=medium

  * New upstream version 2.1
  * Add reference.

 -- Andrius Merkys <merkys@debian.org>  Thu, 31 Mar 2022 05:39:35 -0400

libeuclid-java (2.0-1) unstable; urgency=medium

  * New upstream version 2.0
  * Bumping copyright years.

 -- Andrius Merkys <merkys@debian.org>  Thu, 06 Jan 2022 03:31:01 -0500

libeuclid-java (1.6-1) unstable; urgency=medium

  * New upstream version 1.6
  * Bumping copyright years.

 -- Andrius Merkys <merkys@debian.org>  Sun, 02 Jan 2022 02:01:17 -0500

libeuclid-java (1.4-1) unstable; urgency=medium

  [ Andrius Merkys ]
  * New upstream version 1.4

  [ lintian-brush ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.6.0, no changes needed.

 -- Andrius Merkys <merkys@debian.org>  Sat, 18 Dec 2021 01:33:58 -0500

libeuclid-java (1.3-3) unstable; urgency=medium

  * Watching tags at GitHub.
  * Removing boilerplate debian/README.source.

 -- Andrius Merkys <merkys@debian.org>  Mon, 08 Nov 2021 08:47:46 -0500

libeuclid-java (1.3-2) unstable; urgency=medium

  * Running build time tests.
  * Adding versioned depends on libxom-java to get around #970723.

 -- Andrius Merkys <merkys@debian.org>  Thu, 26 Aug 2021 01:15:33 -0400

libeuclid-java (1.3-1) unstable; urgency=medium

  * New upstream version 1.3
  * Switching from Bitbucket to GitHub.
  * Bumping debhelper-compat (no changes).
  * Adding newline to the end of debian/rules.
  * Removing empty files from debian/.

 -- Andrius Merkys <merkys@debian.org>  Wed, 25 Aug 2021 04:28:17 -0400

libeuclid-java (1.0.1-2) unstable; urgency=medium

  * Adding 'Rules-Requires-Root: no'.

 -- Andrius Merkys <merkys@debian.org>  Tue, 10 Dec 2019 05:30:26 -0500

libeuclid-java (1.0.1-1) unstable; urgency=medium

  * Initial release (Closes: #945136)

 -- Andrius Merkys <merkys@debian.org>  Wed, 20 Nov 2019 05:41:43 -0500
